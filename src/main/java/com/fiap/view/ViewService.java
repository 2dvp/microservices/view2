package com.fiap.view;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ViewService {
    @Autowired
    ViewRepository viewRepository;

    public List<View> getAllViews() {
        List<View> views = new ArrayList<View>();
        viewRepository.findAll().forEach(view -> views.add(view));
        return views;
    }

    public View getViewById(int id) {
        return viewRepository.findById(id).get();
    }

    public void saveOrUpdate(View view) {
        viewRepository.save(view);
    }


    public void delete(int id) {
        viewRepository.deleteById(id);
    }
}