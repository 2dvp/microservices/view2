package com.fiap.view;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "VIEW")
public class View {
    @Id
    @GeneratedValue
    public int id;
    public int product_id; 
    public int view_count;
    public int type_id;
    public Integer getId() {
       return id;
   } 

}